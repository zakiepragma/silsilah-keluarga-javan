import axios from "axios";
import React, { useEffect, useState } from "react";
import Card from "./components/Card";

const App = () => {
  const [keluarga, setKeluarga] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [name, setName] = useState("");
  const [gender, setGender] = useState("");
  const [fatherId, setFatherId] = useState("");
  const [editData, setEditData] = useState(false);
  const [idPerson, setIdPerson] = useState("");
  const [loading, setLoading] = useState(false);

  function showModalCreate() {
    setShowModal(true);
    console.log(idPerson);
  }

  function closeModal() {
    setShowModal(false);
    setForm();
  }

  const setForm = () => {
    setIdPerson("");
    setName("");
    setGender("");
    setFatherId("");
    setEditData(false);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    if (name.trim() === "") {
      alert("Nama tidak boleh kosong!");
      return;
    }
    if (gender === "") {
      alert("Jenis kelamin harus dipilih!");
      return;
    }
    axios
      .post("http://localhost:8000/api/keluarga/", {
        nama: name,
        jenis_kelamin: gender,
        ayah_id: fatherId,
      })
      .then((response) => {
        fetchData();
        setShowModal(false);
      });
  };

  const updateKeluarga = (event) => {
    event.preventDefault();
    if (name.trim() === "") {
      alert("Nama tidak boleh kosong!");
      return;
    }
    if (gender === "") {
      alert("Jenis kelamin harus dipilih!");
      return;
    }
    axios
      .put(`http://localhost:8000/api/keluarga/${idPerson}`, {
        nama: name,
        jenis_kelamin: gender,
        ayah_id: fatherId,
      })
      .then((response) => {
        fetchData();
        setShowModal(false);
        setEditData(false);
        setForm();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  function editKeluarga(id) {
    axios
      .get(`http://localhost:8000/api/keluarga/${id}`)
      .then((response) => {
        setIdPerson(response.data.id);
        setName(response.data.nama);
        setGender(response.data.jenis_kelamin);
        setFatherId(response.data.ayah_id);
        setEditData(true);
        showModalCreate();
      })
      .catch((error) => {
        console.log(error);
      });
  }

  function deleteKeluarga(id) {
    const confirmed = window.confirm(
      "Apakah Anda yakin ingin menghapus data ini?"
    );
    if (confirmed) {
      axios
        .delete(`http://localhost:8000/api/keluarga/${id}`)
        .then((response) => {
          fetchData();
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }

  const fetchData = async () => {
    await axios.get("http://localhost:8000/api/keluarga/").then((response) => {
      setLoading(false);
      setKeluarga(response.data);
      console.log(response.data);
    });
  };

  useEffect(() => {
    setLoading(true);
    fetchData();
  }, []);

  return (
    <div className="container mx-auto">
      {showModal && (
        <div className="fixed z-10 inset-0 overflow-y-auto">
          <div className="flex items-center justify-center min-h-screen p-4">
            <div className="fixed inset-0 bg-gray-500 bg-opacity-75"></div>
            <div className="relative bg-white rounded-lg w-full max-w-md">
              <div className="p-4">
                <h1 className="text-lg font-bold mb-2">
                  {editData ? "Edit Keluarga" : "Create New Familiy"}
                </h1>
                <form onSubmit={editData ? updateKeluarga : handleSubmit}>
                  <div className="mb-4">
                    <label
                      className="block text-gray-700 font-bold mb-2"
                      htmlFor="name"
                    >
                      Nama
                    </label>
                    <input
                      className="appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                      id="name"
                      type="text"
                      placeholder="Masukkan nama"
                      value={name}
                      onChange={(event) => setName(event.target.value)}
                    />
                  </div>
                  <div className="mb-4">
                    <label
                      className="block text-gray-700 font-bold mb-2"
                      htmlFor="gender"
                    >
                      Jenis Kelamin
                    </label>
                    <div>
                      <label className="inline-flex items-center">
                        <input
                          type="radio"
                          className="form-radio"
                          name="gender"
                          value="Laki-laki"
                          checked={gender === "Laki-laki"}
                          onChange={(event) => setGender(event.target.value)}
                        />
                        <span className="ml-2">Laki-laki</span>
                      </label>
                      <label className="inline-flex items-center ml-6">
                        <input
                          type="radio"
                          className="form-radio"
                          name="gender"
                          value="Perempuan"
                          checked={gender === "Perempuan"}
                          onChange={(event) => setGender(event.target.value)}
                        />
                        <span className="ml-2">Perempuan</span>
                      </label>
                    </div>
                  </div>
                  <div className="mb-4">
                    <label
                      className="block text-gray-700 font-bold mb-2"
                      htmlFor="father_id"
                    >
                      Pilih Ayah
                    </label>
                    <select
                      className="appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                      id="father_id"
                      value={fatherId}
                      onChange={(event) => setFatherId(event.target.value)}
                    >
                      <option value="">Pilih Ayah</option>
                      {keluarga.map((item, index) => (
                        <>
                          {(item.jenis_kelamin !== "Perempuan") &
                            (item.id !== idPerson) && (
                            <option key={index} value={item.id}>
                              {item.nama}
                            </option>
                          )}
                        </>
                      ))}
                    </select>
                  </div>
                  <div className="flex items-center justify-end">
                    <button
                      className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                      type="submit"
                    >
                      Submit
                    </button>
                  </div>
                </form>
              </div>
              <div className="flex justify-start gap-2 items-center px-4 py-2 bg-gray-100 border-t border-gray-200">
                <button
                  onClick={() => closeModal()}
                  className="px-2 py-1 text-sm rounded text-white bg-yellow-500 hover:bg-yellow-600 focus:outline-none focus:ring-2 focus:ring-yellow-500 focus:ring-opacity-50"
                >
                  Close
                </button>
                <button
                  onClick={() => setForm()}
                  className="px-2 py-1 text-sm rounded text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-opacity-50"
                >
                  Reset
                </button>
              </div>
            </div>
          </div>
        </div>
      )}
      <div className="flex justify-between items-center pt-10 px-12">
        <p className="text-bold text-4xl ">Silsilah Keluarga</p>
        <button
          onClick={showModalCreate}
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-lg"
        >
          Create New
        </button>
      </div>
      <div className="p-12 grid grid-cols-4 text-center gap-3">
        {keluarga.length <= 0 ? (
          "Data tidak ada..."
        ) : (
          <>
            {loading ? (
              "Loading..."
            ) : (
              <>
                {keluarga.map((item) => (
                  <Card
                    key={item.id}
                    item={item}
                    onEdit={editKeluarga}
                    onDelete={deleteKeluarga}
                  />
                ))}
              </>
            )}
          </>
        )}
      </div>
    </div>
  );
};

export default App;
