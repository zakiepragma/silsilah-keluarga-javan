import React from "react";

const Card = ({ item, onEdit, onDelete }) => {
  return (
    <div className="border rounded shadow p-4 mb-4">
      <h2 className="text-xl font-bold mb-4">{item.title}</h2>
      <p className="text-2xl mb-4">{item.nama}</p>
      <p className="mb-4">{item.jenis_kelamin}</p>
      <p className="mb-4">
        {item.ayah_id != null ? "ayah-nya adalah : " + item.ayah.nama : "-"}
      </p>
      <div className="flex justify-around">
        <button
          className="bg-green-500 text-white px-4 py-2 hover:bg-green-900 rounded-lg"
          onClick={() => onEdit(item.id)}
        >
          Edit
        </button>
        <button
          className="bg-red-500 text-white px-4 py-2 hover:bg-red-900 rounded-lg text-sm"
          onClick={() => onDelete(item.id)}
        >
          Delete
        </button>
      </div>
    </div>
  );
};

export default Card;
